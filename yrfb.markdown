YRFB
----
SVG video mask on text

A [Pen](https://codepen.io/seanshibas/pen/ydXRaB) by [sean](https://codepen.io/seanshibas) on [CodePen](https://codepen.io).

[License](https://codepen.io/seanshibas/pen/ydXRaB/license).